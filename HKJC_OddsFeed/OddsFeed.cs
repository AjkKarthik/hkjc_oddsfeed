﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.Xml.XPath;
using Npgsql;

namespace HKJC_OddsFeed
{
    class OddsFeed
    { 
        public void parsingPreOddsData()

        {
            IConfigurationSection configurationSection = Bettorlogic.HKJC.Utility.GlobalUtility.getConfigdetailsBySection("BearerCredentials");
            string BearerKey = string.Empty;
            string TokenUrl = configurationSection.GetSection("URL").Value;
            string strAuthourization = configurationSection.GetSection("Authorization").Value;
            
            IConfigurationSection configurationSectionEvents = Bettorlogic.HKJC.Utility.GlobalUtility.getConfigdetailsBySection("ProviderMatchesList");
            string EventsUrl = configurationSectionEvents.GetSection("URL").Value;            
            IConfigurationSection configurationSectionMatchInfo = Bettorlogic.HKJC.Utility.GlobalUtility.getConfigdetailsBySection("ProviderMatchesInfo");
            IConfigurationSection configurationSectionMatchOutcomes = Bettorlogic.HKJC.Utility.GlobalUtility.getConfigdetailsBySection("ProviderOutcomesInfo");
            IConfigurationSection configurationSectionPostgres = Bettorlogic.HKJC.Utility.GlobalUtility.getConfigdetailsBySection("connectionStrings");
            string ConnectionString = configurationSectionPostgres.GetSection("ConnectionString").Value;
            NpgsqlConnection conn = new(ConnectionString);

            try 
            {
                string apiResponseKey = Bettorlogic.HKJC.Utility.GlobalUtility.GetFeedDataPost(TokenUrl, strAuthourization);

                if (!string.IsNullOrEmpty(apiResponseKey))
                    {                      
                       JObject json = JObject.Parse(apiResponseKey);
                       BearerKey  = (string)json["access_token"];
                      if (!string.IsNullOrEmpty(BearerKey))
                      {
                        string apiResponseEvents = Bettorlogic.HKJC.Utility.GlobalUtility.GetFeedDataGet(EventsUrl, BearerKey);
                        if (!string.IsNullOrEmpty(apiResponseEvents))
                        {
                            conn.Open();
                            using (var cmd = new NpgsqlCommand("INSERT INTO dbo.providermatchesjsons (inserteddate,jsonfile,processed) VALUES (@eventid,@apiResponseEvents,0)", conn))
                            {
                                cmd.Parameters.AddWithValue("@eventid", DateTime.UtcNow.ToString());
                                cmd.Parameters.AddWithValue("@apiResponseEvents", apiResponseEvents);
                                cmd.ExecuteNonQuery();
                            }
                            conn.Close();
                            XDocument xDoc = XDocument.Parse(apiResponseEvents);
                            var Matchids =
                                from x in xDoc.Root.Descendants()
                                select x.Attribute("id").Value;
                            foreach (var eventid in Matchids)
                            {
                                string MatchUrl = configurationSectionMatchInfo.GetSection("URL").Value;
                                MatchUrl = MatchUrl.Replace("{@MatchId}", eventid);
                                string OutcomeUrl = configurationSectionMatchOutcomes.GetSection("URL").Value;
                                OutcomeUrl = OutcomeUrl.Replace("{@MatchId}", eventid);
                                string apiResponseMatches = Bettorlogic.HKJC.Utility.GlobalUtility.GetFeedDataGet(MatchUrl, BearerKey);

                                string apiResponseOutcomes = Bettorlogic.HKJC.Utility.GlobalUtility.GetFeedDataGet(OutcomeUrl, BearerKey);

                                if (!string.IsNullOrEmpty(apiResponseMatches))
                                {
                                    conn.Open();
                                    using (var cmd = new NpgsqlCommand("INSERT INTO dbo.matchinfojsons (matchid,jsonfile,processed) VALUES (@eventid,@apiResponseMatches,0)", conn))
                                    {
                                        cmd.Parameters.AddWithValue("@eventid", eventid);
                                        cmd.Parameters.AddWithValue("@apiResponseMatches", apiResponseMatches);
                                        cmd.ExecuteNonQuery();
                                    }
                                    conn.Close();
                                }

                                if (!string.IsNullOrEmpty(apiResponseOutcomes))
                                {
                                    conn.Open();
                                    using (var cmd = new NpgsqlCommand("INSERT INTO dbo.matchoutcomesjsons (matchid,jsonfile,processed) VALUES (@eventid,@apiResponseOutcomes,0)", conn))
                                    {
                                        cmd.Parameters.AddWithValue("@eventid", eventid);
                                        cmd.Parameters.AddWithValue("@apiResponseOutcomes", apiResponseOutcomes);
                                        cmd.ExecuteNonQuery();
                                    }
                                    conn.Close();

                                }

                            }

                            conn.Open();
                            using (var cmd = new NpgsqlCommand("CALL dbo.udp_oddsfeed();", conn))
                            {
                                cmd.ExecuteNonQuery();
                            }

                            conn.Close();
                        }

                      }
                    }
                    
                    
            }
            catch (Exception ex) {
                Bettorlogic.HKJC.Utility.GlobalUtility.Logger.Info(ex);
                conn.Close();
            }
            finally {
                conn.Close();
            }

        }


    }
}
