﻿using System;
using System.IO;
using System.Reflection;
using Bettorlogic.HKJC.Utility;
using Microsoft.Extensions.Configuration;

namespace HKJC_OddsFeed
{
    class Program
    {
        static void Main(string[] args)
        {
            IConfigurationSection configurationSection = Bettorlogic.HKJC.Utility.GlobalUtility.getConfigdetailsBySection("OddsFeedConfig");
            bool OddsFeed = Convert.ToBoolean(configurationSection.GetSection("OddsFeed").Value);
            bool ResultsFeed = Convert.ToBoolean(configurationSection.GetSection("ResultsFeed").Value);
            bool CntryLeagueTeamPlayerFeed = Convert.ToBoolean(configurationSection.GetSection("CntryLeagueTeamPlayerFeed").Value);
            var log4netRepository = log4net.LogManager.GetRepository(Assembly.GetEntryAssembly());
            log4net.Config.XmlConfigurator.Configure(log4netRepository, new FileInfo("log4net.config"));
            Bettorlogic.HKJC.Utility.GlobalUtility.Logger.Info("Start oddsfeed");
            if (OddsFeed)
            {
                OddsFeed objOddsFeed = new OddsFeed();
                objOddsFeed.parsingPreOddsData();
            }
            if (ResultsFeed)
            {
                ResultFeed objOddsFeed = new ResultFeed();
                objOddsFeed.parsingPreOddsData();
            }
            if (CntryLeagueTeamPlayerFeed)
            {
                CntryLeagueTeamPlayerFeed objOddsFeed = new CntryLeagueTeamPlayerFeed();
                objOddsFeed.parsingPreOddsData();
            }

        }
    }
}

