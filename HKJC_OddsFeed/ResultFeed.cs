﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace HKJC_OddsFeed
{
    class ResultFeed
    {
        public void parsingPreOddsData()

        {
            IConfigurationSection configurationSection = Bettorlogic.HKJC.Utility.GlobalUtility.getConfigdetailsBySection("BearerCredentials");
            string BearerKey = string.Empty;
            string TokenUrl = configurationSection.GetSection("URL").Value;
            string strAuthourization = configurationSection.GetSection("Authorization").Value;

            IConfigurationSection configurationSectionResultsMatchesList = Bettorlogic.HKJC.Utility.GlobalUtility.getConfigdetailsBySection("ProviderResultsMatchesList");
            string ResultsMatchesListUrl = configurationSectionResultsMatchesList.GetSection("URL").Value;
            int noofdays = Convert.ToInt32(configurationSectionResultsMatchesList.GetSection("noofdays").Value);

            IConfigurationSection configurationSectionPostgres = Bettorlogic.HKJC.Utility.GlobalUtility.getConfigdetailsBySection("connectionStrings");
            string ConnectionString = configurationSectionPostgres.GetSection("ConnectionString").Value;
            NpgsqlConnection conn = new(ConnectionString);
            IConfigurationSection configurationSectionMatchInfo = Bettorlogic.HKJC.Utility.GlobalUtility.getConfigdetailsBySection("ProviderMatchesInfo");
            IConfigurationSection configurationSectionMatchOutcomes = Bettorlogic.HKJC.Utility.GlobalUtility.getConfigdetailsBySection("ProviderOutcomesInfo");

            try
            {
                string apiResponseKey = Bettorlogic.HKJC.Utility.GlobalUtility.GetFeedDataPost(TokenUrl, strAuthourization);

                if (!string.IsNullOrEmpty(apiResponseKey))
                {
                    JObject json = JObject.Parse(apiResponseKey);
                    BearerKey = (string)json["access_token"];
                    if (!string.IsNullOrEmpty(BearerKey))
                    {
                        string date = DateTime.Now.AddDays(-noofdays).ToString("yyyyMMdd");
                        ResultsMatchesListUrl = ResultsMatchesListUrl.Replace("{Date}", date).Replace("{noofdays}", (noofdays+1).ToString());
                        string apiResponseResultsMatchesList = Bettorlogic.HKJC.Utility.GlobalUtility.GetFeedDataGet(ResultsMatchesListUrl, BearerKey);
                        if (!string.IsNullOrEmpty(apiResponseResultsMatchesList))
                        {
                            conn.Open();
                            using (var cmd = new NpgsqlCommand("INSERT INTO dbo.Resultmatchinfojsons (inserteddate,jsonfile,processed) VALUES (@eventid,@apiResponseResultsMatchesList,0)", conn))
                            {
                                cmd.Parameters.AddWithValue("@eventid", DateTime.UtcNow.ToString());
                                cmd.Parameters.AddWithValue("@apiResponseResultsMatchesList", apiResponseResultsMatchesList);
                                cmd.ExecuteNonQuery();
                            }
                            using (var cmd = new NpgsqlCommand("CALL dbo.udp_resultmatchinfojsons();", conn))
                            {
                                cmd.ExecuteNonQuery();
                            }
                            conn.Close();
                            
                        }
                           DataSet dsmatchesproviderresults = new ();
                           DataSet dsmatchescorners = new();

                            conn.Open();
                            NpgsqlDataAdapter damatchesproviderresults = new ("SELECT * from dbo.udp_getmatchesforproviderresults();", conn);
                            damatchesproviderresults.Fill(dsmatchesproviderresults);
                            NpgsqlDataAdapter damatchescorners = new("select * from dbo.udp_getmatchesforcorners();", conn);
                            damatchescorners.Fill(dsmatchescorners);
                            conn.Close();
                        
                        if (dsmatchesproviderresults != null && dsmatchesproviderresults.Tables.Count > 0)
                        {
                            for (int i = 0; i < dsmatchesproviderresults.Tables[0].Rows.Count; i++)
                            {
                                string matchid = dsmatchesproviderresults.Tables[0].Rows[i]["matchids"].ToString();
                                string MatchUrl = configurationSectionMatchOutcomes.GetSection("URL").Value;
                                MatchUrl = MatchUrl.Replace("{@MatchId}", matchid);
                                string apiResponseResultsMatches = Bettorlogic.HKJC.Utility.GlobalUtility.GetFeedDataGet(MatchUrl, BearerKey);
                                if (!string.IsNullOrEmpty(apiResponseResultsMatches))
                                {
                                    conn.Open();
                                    using (var cmd = new NpgsqlCommand("INSERT INTO dbo.providerresultjsons (inserteddate,jsonfile,processed) VALUES (@eventid,@apiResponseResultsMatches,0)", conn))
                                    {
                                        cmd.Parameters.AddWithValue("@eventid", DateTime.UtcNow.ToString());
                                        cmd.Parameters.AddWithValue("@apiResponseResultsMatches", apiResponseResultsMatches);
                                        cmd.ExecuteNonQuery();
                                    }
                                    using (var cmd = new NpgsqlCommand("CALL dbo.udp_providerresultjsons;", conn))
                                    {
                                        cmd.ExecuteNonQuery();
                                    }
                                    conn.Close();


                                }

                            }
                        }
                        if (dsmatchescorners != null && dsmatchescorners.Tables.Count > 0)
                        {
                            for (int i = 0; i < dsmatchescorners.Tables[0].Rows.Count; i++)
                            {
                                string matchid = dsmatchescorners.Tables[0].Rows[i]["matchids"].ToString();
                                string MatchUrl = configurationSectionMatchInfo.GetSection("URL").Value;
                                MatchUrl = MatchUrl.Replace("{@MatchId}", matchid);
                                string apiResponsecorners = Bettorlogic.HKJC.Utility.GlobalUtility.GetFeedDataGet(MatchUrl, BearerKey);
                                if (!string.IsNullOrEmpty(apiResponsecorners))
                                {
                                    conn.Open();
                                    using (var cmd = new NpgsqlCommand("INSERT INTO dbo.cornersjsons (inserteddate,jsonfile,processed) VALUES (@eventid,@apiResponsecorners,0)", conn))
                                    {
                                        cmd.Parameters.AddWithValue("@eventid", DateTime.UtcNow.ToString());
                                        cmd.Parameters.AddWithValue("@apiResponsecorners", apiResponsecorners);
                                        cmd.ExecuteNonQuery();
                                    }
                                    using (var cmd = new NpgsqlCommand("CALL dbo.udp_cornersjsons();", conn))
                                    {
                                        cmd.ExecuteNonQuery();
                                    }

                                    conn.Close();

                                }

                            }
                        }


                    }
               
                }


            }
            catch (Exception ex)
            {
                Bettorlogic.HKJC.Utility.GlobalUtility.Logger.Info(ex);
                conn.Close();
            }
            finally
            {
                conn.Close();
            }

        }

    }
}
