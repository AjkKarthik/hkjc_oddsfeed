﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace HKJC_OddsFeed
{
    class CntryLeagueTeamPlayerFeed
    {
        public void parsingPreOddsData()

        {
            IConfigurationSection configurationSection = Bettorlogic.HKJC.Utility.GlobalUtility.getConfigdetailsBySection("BearerCredentials");
            string BearerKey = string.Empty;
            string TokenUrl = configurationSection.GetSection("URL").Value;
            string strAuthourization = configurationSection.GetSection("Authorization").Value;
            IConfigurationSection configurationSectionPostgres = Bettorlogic.HKJC.Utility.GlobalUtility.getConfigdetailsBySection("connectionStrings");
            string ConnectionString = configurationSectionPostgres.GetSection("ConnectionString").Value;
            NpgsqlConnection conn = new(ConnectionString);
            IConfigurationSection configurationSectionProviderteams = Bettorlogic.HKJC.Utility.GlobalUtility.getConfigdetailsBySection("Providerteams");
            IConfigurationSection configurationSectionProvidercountries = Bettorlogic.HKJC.Utility.GlobalUtility.getConfigdetailsBySection("Providercountries");
            IConfigurationSection configurationSectionProviderLeagues = Bettorlogic.HKJC.Utility.GlobalUtility.getConfigdetailsBySection("ProviderLeagues");
            IConfigurationSection configurationSectionProviderplayer = Bettorlogic.HKJC.Utility.GlobalUtility.getConfigdetailsBySection("Providerplayer");
            string ProviderteamsUrl = configurationSectionProviderteams.GetSection("URL").Value;
            string ProvidercountriesUrl = configurationSectionProvidercountries.GetSection("URL").Value;
            string ProviderLeaguesUrl = configurationSectionProviderLeagues.GetSection("URL").Value;
            


            try
            {
                string apiResponseKey = Bettorlogic.HKJC.Utility.GlobalUtility.GetFeedDataPost(TokenUrl, strAuthourization);

                if (!string.IsNullOrEmpty(apiResponseKey))
                {
                    JObject json = JObject.Parse(apiResponseKey);
                    BearerKey = (string)json["access_token"];
                    if (!string.IsNullOrEmpty(BearerKey))
                    {
                        string apiResponseProviderteams = Bettorlogic.HKJC.Utility.GlobalUtility.GetFeedDataGet(ProviderteamsUrl, BearerKey);

                        string apiResponseProvidercountries = Bettorlogic.HKJC.Utility.GlobalUtility.GetFeedDataGet(ProvidercountriesUrl, BearerKey);

                        string apiResponseProviderLeagues = Bettorlogic.HKJC.Utility.GlobalUtility.GetFeedDataGet(ProviderLeaguesUrl, BearerKey);

                        if (!string.IsNullOrEmpty(apiResponseProviderteams))
                        {

                            conn.Open();
                            using (var cmd = new NpgsqlCommand("INSERT INTO dbo.providerteamsjsons (inserteddate,jsonfile,processed) VALUES (@eventid,@apiResponseProviderteams,0)", conn))
                            {
                                cmd.Parameters.AddWithValue("@eventid", DateTime.UtcNow.ToString());
                                cmd.Parameters.AddWithValue("@apiResponseProviderteams", apiResponseProviderteams);
                                cmd.ExecuteNonQuery();
                            }
                            using (var cmd = new NpgsqlCommand("CALL dbo.udp_providerteamsjsons();", conn))
                            {
                                cmd.ExecuteNonQuery();
                            }
                            conn.Close();

                        }

                        if (!string.IsNullOrEmpty(apiResponseProvidercountries))
                        {
                            conn.Open();
                            using (var cmd = new NpgsqlCommand("INSERT INTO dbo.providercountriesjsons (inserteddate,jsonfile,processed) VALUES (@eventid,@apiResponseProvidercountries,0)", conn))
                            {
                                cmd.Parameters.AddWithValue("@eventid", DateTime.UtcNow.ToString());
                                cmd.Parameters.AddWithValue("@apiResponseProvidercountries", apiResponseProvidercountries);
                                cmd.ExecuteNonQuery();
                            }
                            using (var cmd = new NpgsqlCommand("CALL dbo.udp_providercountriesjsons();", conn))
                            {
                                cmd.ExecuteNonQuery();
                            }
                            conn.Close();

                        }
                        if (!string.IsNullOrEmpty(apiResponseProviderLeagues))
                        {
                            conn.Open();
                            using (var cmd = new NpgsqlCommand("INSERT INTO dbo.providerleaguesjsons (inserteddate,jsonfile,processed) VALUES (@eventid,@apiResponseProviderLeagues,0)", conn))
                            {
                                cmd.Parameters.AddWithValue("@eventid", DateTime.UtcNow.ToString());
                                cmd.Parameters.AddWithValue("@apiResponseProviderLeagues", apiResponseProviderLeagues);
                                cmd.ExecuteNonQuery();
                            }
                            using (var cmd = new NpgsqlCommand("CALL dbo.udp_providerleaguesjsons();", conn))
                            {
                                cmd.ExecuteNonQuery();
                            }
                            conn.Close();

                        }

                        for (char c = 'A'; c <= 'Z'; c++)
                        {
                            string ProviderplayerUrl = configurationSectionProviderplayer.GetSection("URL").Value;
                            ProviderplayerUrl = ProviderplayerUrl.Replace("{char}", c.ToString());

                            string apiResponseProviderPlayers = Bettorlogic.HKJC.Utility.GlobalUtility.GetFeedDataGet(ProviderplayerUrl, BearerKey);
                            if (!string.IsNullOrEmpty(apiResponseProviderPlayers))
                            {
                                conn.Open();
                                using (var cmd = new NpgsqlCommand("INSERT INTO dbo.providerplayersjsons (inserteddate,jsonfile,processed) VALUES (@eventid,@apiResponseProviderPlayers,0)", conn))
                                {
                                    cmd.Parameters.AddWithValue("@eventid", DateTime.UtcNow.ToString());
                                    cmd.Parameters.AddWithValue("@apiResponseProviderPlayers", apiResponseProviderPlayers);
                                    cmd.ExecuteNonQuery();
                                }

                                conn.Close();
                            }


                        }
                        conn.Open();
                        using (var cmd = new NpgsqlCommand("CALL dbo.udp_providerplayersjsons();", conn))
                        {
                            cmd.ExecuteNonQuery();
                        }

                        conn.Close();


                    }

                }


            }
            catch (Exception ex)
            {
                Bettorlogic.HKJC.Utility.GlobalUtility.Logger.Info(ex);
                conn.Close();
            }
            finally
            {
                conn.Close();
            }

        }

    }

}
