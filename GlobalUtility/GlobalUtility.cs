﻿using log4net;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Bettorlogic.HKJC.Utility
{
    public class GlobalUtility
    {
        private static ILog _logger;

        public static ILog Logger
        {
            get
            {
                if (_logger == null)
                {
                    log4net.Config.XmlConfigurator.Configure();
                    _logger = log4net.LogManager.GetLogger("HKJCLog");
                }
                return _logger;
            }
        }

        public static IConfigurationRoot configfile()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("Appsettings.json");

            var configuration = builder.Build();
            return configuration;
        }


        public static IConfigurationSection getConfigdetailsBySection(string section)
        {
            var configuration = GlobalUtility.configfile();
            return configuration.GetSection(section);
        }

        public static string GetFeedDataGet(string Url, string strAuthourization)
        {
            string strResponse = string.Empty;
            Bettorlogic.HKJC.Utility.GlobalUtility.Logger.Info("Oddsfeed Url" + Url);
            try
            {
                using (var wc = new System.Net.WebClient())
                {

                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Tls13 | SecurityProtocolType.SystemDefault;
                    wc.Headers.Add("Authorization", "Bearer " + strAuthourization);
                    strResponse = wc.DownloadString(Url);
                }
            }
            catch (Exception ex)
            {
                Bettorlogic.HKJC.Utility.GlobalUtility.Logger.Info("Oddsfeed Url" + ex);
            }

            return strResponse;
        }

        public static string GetFeedDataPost(string Url, string strAuthourization)
        {
            string strResponse = string.Empty;
            Bettorlogic.HKJC.Utility.GlobalUtility.Logger.Info("Oddsfeed Url" + Url);
            try
            {
                using (var wc = new System.Net.WebClient())
                {
                    string postStr = "{}";
                    byte[] byteArray = Encoding.UTF8.GetBytes(postStr);
                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Tls13 | SecurityProtocolType.SystemDefault;
                    wc.Headers.Add("Authorization", strAuthourization);
                    //strResponse = wc.DownloadString(Url);
                    byte[] Response = wc.UploadData(Url, "POST", byteArray);
                    strResponse = Encoding.ASCII.GetString(Response);

                }

            }

            catch (Exception ex)
            {
                Bettorlogic.HKJC.Utility.GlobalUtility.Logger.Info("Oddsfeed Url" + ex);
            }

            return strResponse;
        }

    }
}
